using System;

using System.Linq;
namespace debate.model
{
    // creates a context for all the tests
    public class TestData
    {
        public static dbArgResolverContext fixedContext = new dbArgResolverContext();

        public static void createTestData()
        {

           
          createTestArguments();
        }

        public static void createTestArguments()
        {
            Arguments vscode = new Arguments(
                Guid.NewGuid().ToString(),
                fixedContext.Users.FirstOrDefault(user => user.Username == "pen").Code,
                "test",
                "vscode is the best ide",
                "light, capable and easily customisable",
                DateTime.Now.AddMonths(2)
            );

            Arguments vegan = new Arguments(
                          Guid.NewGuid().ToString(),
                          fixedContext.Users.FirstOrDefault(user => user.Username == "logan").Code,
                          "test",
                          "It's wrong to eat meat",
                          "Humans are fully capable of living healthy lives without killing animals",
                          DateTime.Now.AddMonths(2)
                      );
           

            vscode.saveArgument();
            vegan.saveArgument();
       }
        public static void createTestUsers()
        {
            // create users
            Users edwardo = new Users("edwardo", "edwardo");
            Users pen = new Users("pen", "pen");
            Users logan = new Users("logan", "logan");
            Users maleni = new Users("maleni", "maleni");
            Users earnest = new Users("earnest", "earnest");
            Users ocean = new Users("ocean", "mrwavey");

            fixedContext.Users.Add(edwardo);
            fixedContext.Users.Add(pen);
            fixedContext.Users.Add(logan);
            fixedContext.Users.Add(maleni);
            fixedContext.Users.Add(earnest);
            fixedContext.Users.Add(ocean);

            fixedContext.SaveChanges();
        }

        public static void cleanDb()
        {

            // clean all tables 
            if (fixedContext.Users.Count() > 0)
            {
                fixedContext.Users.RemoveRange(fixedContext.Users);
                fixedContext.SaveChanges();
            }
            if (fixedContext.Arguments.Count() > 0)
            {
                fixedContext.Arguments.RemoveRange(fixedContext.Arguments);
                fixedContext.SaveChanges();
            }
            if (fixedContext.ContributionRequests.Count() > 0)
            {
                fixedContext.ContributionRequests.RemoveRange(fixedContext.ContributionRequests);
                fixedContext.SaveChanges();
            }

        }

    }
}