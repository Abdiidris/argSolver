using System;
using System.Collections.Generic;

using System.Linq;
namespace debate.model
{
    public partial class ArgumentRating
    {
        /*
         *  --------------
         *  | Properties |
         *  --------------
         */

        private dbArgResolverContext context;
        public string UserCode { get; set; }
        public string ArgumentCode { get; set; }

        public bool up { get; set; }

        public Arguments RatedArgmentCodeNavigation { get; set; }
        public ArgumentRating()
        {
            context = new dbArgResolverContext();
        }
        public void addArgumentRating(string argumentRaterCode, string RatedArgumentCode, bool votedUp)
        {

            // if the user has already given this rating to the argument then do nothing otherwise replace current rating
            // check if the user has already given a rating
            List<ArgumentRating> queryResult = (from ratings in context.ArgumentRating
                                                where ratings.ArgumentCode == RatedArgumentCode && ratings.UserCode == argumentRaterCode
                                                select ratings).ToList();

            if (queryResult.Count == 0) // user hasnt voted before so upload a new vote
            {
                UserCode = argumentRaterCode;
                ArgumentCode = RatedArgumentCode;
                up = votedUp;

                context.ArgumentRating.Add(this);
                context.SaveChanges();
            }
            else // user has voted before
            {
                if (queryResult[0].up != votedUp)
                {
                    // replace current vote
                    context.ArgumentRating.Remove(queryResult[0]);

                    UserCode = argumentRaterCode;
                    ArgumentCode = RatedArgumentCode;
                    up = votedUp;

                    context.ArgumentRating.Add(this);
                    context.SaveChanges();
                }
            }




        }
    }
}
