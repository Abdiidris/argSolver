﻿using System;
using System.Collections.Generic;

using System.Data.SqlClient;
using System.Linq;

using Microsoft.EntityFrameworkCore;
namespace debate.model
{
    public partial class Users
    {


        /*
         *  --------------
         *  | Properties |
         *  --------------
         */

        public string Code { get; private set; }
        public string Username { get; private set; }
        public string UserPassword { get; private set; }

        public ICollection<Arguments> Arguments { get; private set; }
        public ICollection<ContributionRequests> ContributionRequests { get; private set; }
        private dbArgResolverContext context;
        /*
         *  --------------
         *  | Methods |
         *  --------------
         */
        public Users(String Username, string UserPassword)
        {
            this.Username = Username;
            this.UserPassword = UserPassword;
            // ef
            Arguments = new HashSet<Arguments>();
            context = new dbArgResolverContext();
        }
        public void uploadUser()
        {
            Code = Guid.NewGuid().ToString("N").ToUpper();

            context.Users.Add(this);
            context.SaveChanges();

        }
        public static Users getUserUsername(string searchUsername)
        {
            return new dbArgResolverContext().Users.FirstOrDefault(user => user.Username == searchUsername);


        }
        public static string getUserCode(string Username)
        {

            string queryResult = (from users in new dbArgResolverContext().Users
                                  where users.Username == Username
                                  select users.Code).ToList()[0].ToString();
            return queryResult;
        }
        public static Users getUserWithCode(string code)
        {


            List<Users> queryResult = (from users in new dbArgResolverContext().Users
                                       where users.Code == code
                                       select users).ToList();

            if (queryResult.Count == 1)
            {
                return queryResult[0];
            }
            else
            {
                return null;
            }
        }
        public void removeUser()
        {

        }
        public List<ContributionRequests> getPendingContributionRequests()
        {
            // Get an return all the requests that this user needs to act on

            List<ContributionRequests> queryResult = (from requests in context.ContributionRequests
                                                      where requests.voterCode == this.Code
                                                      select requests).ToList();
            return queryResult;

        }

    }
}
