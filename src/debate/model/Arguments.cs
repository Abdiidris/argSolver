﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
namespace debate.model
{
    public partial class Arguments // should be just argument not arguments cus it represents 1 argument
    {
        #region efProperties
        private dbArgResolverContext context;
        public ICollection<ContributionRequests> ContributionRequests { get; set; }
        public ICollection<Arguments> Responses { get; set; }
        public ICollection<ArgumentRating> ArgumentRating { get; set; }
        public Users PosterNavigation { get; set; }
        public ICollection<Evidence>    Evidence { get; set; }
        #endregion efProperties

        #region argumentProperties
        public string Code { get; private set; }
        public string PosterCode { get; set; }
        public string Category { get; private set; }
        public int? NumOfViews { get; private set; }
        public string ArgumentPayload { get; private set; }
        public string ArgumentExplanation { get; private set; }
        public DateTime? DatePosted { get; private set; }
        public DateTime? ClosingDate { get; private set; }


        public Arguments(string code, string PosterCode, string Category, string ArgumentPayload, string ArgumentExplanation, DateTime? ClosingDate)
        {
            this.Code = code;
            this.PosterCode = PosterCode;
            this.Category = Category;
            this.NumOfViews = 0;
            this.ArgumentPayload = ArgumentPayload;
            this.ArgumentExplanation = ArgumentExplanation;
            this.DatePosted = DateTime.Now;
            this.ClosingDate = ClosingDate;


            Evidence = new HashSet<Evidence>();

            context = new dbArgResolverContext();

        }

        #endregion
        #region responseProperties

        public Arguments RespondingToArgmentNavigation { get; set; } // this is the argument this argument is responsing to

        public bool forArgument { get; private set; }

        public bool? requestAccepted { get; private set; }

        public string respondingToCode { get; private set; }

        // responce constructor
        public Arguments(string code, string PosterCode, string Category, string ArgumentPayload, string ArgumentExplanation, DateTime ClosingDate, bool forArgument, string respondingToCode)
        {
            this.Code = code;
            this.PosterCode = PosterCode;
            this.Category = Category;
            this.NumOfViews = 0;
            this.ArgumentPayload = ArgumentPayload;
            this.ArgumentExplanation = ArgumentExplanation;
            this.ClosingDate = ClosingDate;

            this.respondingToCode = respondingToCode;
            this.forArgument = forArgument;
            this.requestAccepted = true;

            // create contribution requests

            Evidence = new HashSet<Evidence>();
            context = new dbArgResolverContext();
        }
        #endregion
        #region staticMethods


        public static Arguments getWithCode(string code)
        {

            List<Arguments> queryResult = (from arguments in new dbArgResolverContext().Arguments
                                           where arguments.Code == code
                                           select arguments).ToList();

            if (queryResult.Count == 1)
            {
                return queryResult[0];
            }
            else
            {
                return null;
            }

        }

        public static List<Arguments> getArgumentsByUser(string userName)
        {
            List<Arguments> queryResult = (from arguments in new dbArgResolverContext().Arguments
                                           where arguments.PosterCode == Users.getUserCode(userName)
                                           select arguments).ToList();
            return queryResult;
        }


        public static List<Arguments> getArgumentByCategory(string category)
        {
            List<Arguments> queryResult = (from arguments in new dbArgResolverContext().Arguments
                                           where arguments.Category == category
                                           select arguments).ToList();
            return queryResult;
        }
        public static List<Arguments> getAllArguments()
        {
            List<Arguments> queryResult = (from arguments in new dbArgResolverContext().Arguments
                                           select arguments).ToList();
            return queryResult;
        }
        #endregion
        #region nonStaticMethods
        public bool equals(Arguments comparison)
        {
            return (
                    comparison.Code == this.Code
                    && comparison.PosterCode == this.PosterCode
                    && comparison.Category == this.Category
                    );
        }
        public List<Arguments> GetAgainstResponses()
        {

            // only get responses that have been accepted
            List<Arguments> responses = (from respos in context.Arguments.Include("RespondingToArgmentNavigation")
                                         where respos.respondingToCode == this.Code && respos.requestAccepted == true && respos.forArgument == false
                                         select respos).ToList();
            return responses;
        }
        public List<Arguments> GetForResponses()
        {

            // only get responses that have been accepted
            List<Arguments> responses = (from respos in context.Arguments.Include("RespondingToArgmentNavigation")
                                         where respos.respondingToCode == this.Code && respos.requestAccepted == true && respos.forArgument == true
                                         select respos).ToList();
            return responses;
        }
        public List<Arguments> GetResponses()
        {

            // only get responses that have been accepted
            List<Arguments> responses = (from respos in context.Arguments.Include("RespondingToArgmentNavigation")
                                         where respos.respondingToCode == this.Code && respos.requestAccepted == true
                                         select respos).ToList();
            return responses;
        }
        public void saveArgument()
        {

            // save the argument
            if (this.respondingToCode != null)
            {
                this.createContributionRequest();
            }
            context.Arguments.Add(this);
            context.SaveChanges();

        }
        public string getPosterUserName()
        {

            return (from arg in context.Arguments.Include("PosterNavigation")
                    where arg.Code == this.Code
                    select arg).FirstOrDefault().PosterNavigation.Username;

        }

        public int getUpVotes()
        {
            List<ArgumentRating> queryResult = (from ratings in context.ArgumentRating
                                                where ratings.ArgumentCode == this.Code && ratings.up /*is==true */
                                                select ratings).ToList();


            return queryResult.Count;

        }
        public int getDownVotes()
        {
            List<ArgumentRating> queryResult = (from ratings in context.ArgumentRating
                                                where ratings.ArgumentCode == this.Code && !ratings.up /*is==true */
                                                select ratings).ToList();


            return queryResult.Count;

        }

        #endregion
        #region responseMethods


        public Tuple<List<Arguments>, List<Arguments>> getArgumentsFromEachSide()
        {
            // return values
            List<Arguments> forArguments = new List<Arguments>();
            List<Arguments> againstArguments = new List<Arguments>();

            // get the responses for this argument
            // seperate for and against
            // find a better way of doing this


            var responseLoadedArg = GetResponses();

            foreach (var response in responseLoadedArg)
            {

                // first get the side it is on
                if (response.forArgument)
                {
                    // get the argument for this response

                    forArguments.Add(getWithCode(response.Code)); // returns the argument attached to this response
                }
                else
                {   // get the argument against this response

                    againstArguments.Add(getWithCode(response.Code));
                }
            }




            //  List<R
            return new Tuple<List<Arguments>, List<Arguments>>(forArguments, againstArguments);
        }
        // create the contribution requests
        public void createContributionRequest()
        {
            /*
            get the side that it concerns
            */

            // this will get all the responses already made to the argument this argument is responding
            // load respondee
            Arguments respondingToArg = context.Arguments.FirstOrDefault(a => a.Code == this.respondingToCode);

            Tuple<List<Arguments>, List<Arguments>> sortedResponses = respondingToArg.getArgumentsFromEachSide();


            //   Include("RespondingToArgmentNavigation"); // load RespondingToArgmentNavigation so that it can be used
            //    context.Arguments.Include("Responses");

            // create a contribution request if this is not the first response
            bool firstResponse = false;
            // the voters it concerns
            List<string> RequiredContributerCodes = new List<string>();
            if (forArgument)
            {
                //add the person who created the argument as a For Arguer
                RequiredContributerCodes.Add(respondingToArg.PosterCode);
                RequiredContributerCodes.AddRange(respondingToArg.getForArguers());

            }
            else
            {

                if (sortedResponses.Item2 == null)
                {
                    firstResponse = true;
                }
                else
                {
                    RequiredContributerCodes.AddRange(respondingToArg.getAgainstArguers());
                }
            }

            // dont create a request if this is the first reponse
            if (firstResponse)
            {
                this.requestAccepted = true;
            }
            else
            {

                foreach (var item in RequiredContributerCodes)
                {
                    ContributionRequests newRequest = new ContributionRequests()
                    {
                        responseCode = this.Code,
                        voterCode = item,
                    };

                    context.ContributionRequests.Add(newRequest);
                }
            }

        }

        public List<string> getForArguers()
        {

            // get all for arguments
            List<Arguments> forArguments = getArgumentsFromEachSide().Item1;

            // get the posters codes
            List<string> usersVotedFor = (from forArguers in forArguments
                                          select forArguers.PosterCode).ToList();


            return usersVotedFor;
        }
        public List<string> getAgainstArguers()
        {

            // get against for arguments
            List<Arguments> againstArguments = getArgumentsFromEachSide().Item2;

            // get the posters codes

            List<string> usersVotedFor = (from againstvoters in againstArguments
                                          select againstvoters.PosterCode).ToList();
            return usersVotedFor;
        }
        #endregion
    }
}
