﻿using System;
using System.Collections.Generic;

namespace debate.model
{
    public partial class ContributionRequests
    {
        /*
         *  --------------
         *  | Properties |
         *  --------------
         */

        public string responseCode { get; set; }
        public string voterCode { get; set; }
        public bool? accept { get; set; }
        
        public Arguments responseNavigation { get; set; }
/*
         *  --------------
         *  | Methods |
         *  --------------
         */

       
    }
}
