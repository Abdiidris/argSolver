﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.IO;
namespace debate.model
{
    public partial class dbArgResolverContext : DbContext
    {
        public dbArgResolverContext()
        {
            this.Database.EnsureCreated();


        }
        public dbArgResolverContext(DbContextOptions<dbArgResolverContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ArgumentRating> ArgumentRating { get; set; }

        public virtual DbSet<Arguments> Arguments { get; set; }
        public virtual DbSet<ContributionRequests> ContributionRequests { get; set; }
        public virtual DbSet<Evidence> Evidence { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
          
                
            if (!optionsBuilder.IsConfigured)
            {
                //UPDATE: use secrets when
                optionsBuilder.UseMySql(File.ReadAllText("/home/abdiyee/.microsoft/constring.txt"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ArgumentRating>(entity =>
            {
                entity.HasKey(e => new { e.ArgumentCode, e.UserCode });

                entity.Property(e => e.ArgumentCode)
                    .HasColumnName("argumentCode")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UserCode)
                    .HasColumnName("userCode")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.RatedArgmentCodeNavigation)
                .WithMany(p => p.ArgumentRating)
                .HasForeignKey(d => d.ArgumentCode)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("fkRatedArgument");
            });

            modelBuilder.Entity<Arguments>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.HasIndex(e => e.PosterCode)
                    .HasName("posterCode");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ArgumentExplanation)
                    .HasColumnName("argumentExplanation")
                    .HasColumnType("varchar(2000)");

                entity.Property(e => e.ArgumentPayload)
                    .HasColumnName("argumentPayload")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Category)
                    .HasColumnName("category")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ClosingDate)
                    .HasColumnName("closingDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.DatePosted)
                    .HasColumnName("datePosted")
                    .HasColumnType("datetime");

                entity.Property(e => e.NumOfViews)
                    .HasColumnName("numOfViews")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PosterCode)
                    .IsRequired()
                    .HasColumnName("posterCode")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.PosterNavigation)
                    .WithMany(p => p.Arguments)
                    .HasForeignKey(d => d.PosterCode)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fkPosterCode");

                // response data

                entity.Property(e => e.forArgument)
               .HasColumnName("forArgument")
               .HasColumnType("Bool");

                entity.Property(e => e.requestAccepted)
               .HasColumnName("requestAccepted")
               .HasColumnType("Bool");

                entity.HasOne(d => d.RespondingToArgmentNavigation)
               .WithMany(p => p.Responses)
               .HasForeignKey(d => d.respondingToCode)
               .OnDelete(DeleteBehavior.Cascade)
               .HasConstraintName("ArgumentResponse_ibfk_1");
            });

            modelBuilder.Entity<ContributionRequests>(entity =>
            {
                entity.HasKey(e => new { e.voterCode, e.responseCode });
                
                entity.HasOne(d => d.responseNavigation) 
                  .WithMany(p => p.ContributionRequests)
                  .HasForeignKey(d => d.responseCode)
                  .OnDelete(DeleteBehavior.Cascade)
                  .HasConstraintName("fkResponseCode");

            });

            modelBuilder.Entity<Evidence>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.HasIndex(e => e.ArgumentCode)
                    .HasName("argumentCode");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ArgumentCode)
                    .IsRequired()
                    .HasColumnName("argumentCode")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.EvidencePayload)
                    .HasColumnName("evidencePayload")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.ArgumentCodeNavigation)
                    .WithMany(p => p.Evidence)
                    .HasForeignKey(d => d.ArgumentCode)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fkEvidencesArgument");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UserPassword)
                    .IsRequired()
                    .HasColumnName("userPassword")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasColumnType("varchar(255)");
            });
        }
    }
}
