﻿using System;
using System.Collections.Generic;

namespace debate.model
{
    public partial class Evidence
    {
        /*
         *  --------------
         *  | Properties |
         *  --------------
         */

        public string Code { get; set; }
        public string ArgumentCode { get; set; }
        public string EvidencePayload { get; set; }

        public Arguments ArgumentCodeNavigation { get; set; }



        /*
         *  --------------
         *  | Methods |
         *  --------------
         */

          public void createEvidence(string passArgumentCode)
        {
             Code = Guid.NewGuid().ToString("N").ToUpper();
            ArgumentCode = passArgumentCode;

            System.Console.WriteLine("What is your evidence");
            EvidencePayload = Console.ReadLine();

            
      //   DataLib.save("evidence",  this);
        }
    }
}
