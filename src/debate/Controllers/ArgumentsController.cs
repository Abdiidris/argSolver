using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;
using debate.model;
using System.Threading;

namespace debate.Controllers
{

    [Route("api/[controller]")]
    public class ArgumentsController : Controller
    { 
      
        // [HttpGet("[action]")]
        // public JArray searchArgsByTerm(string term)
        // { 
        //     /*
        //     1. Usernames
        //     2. words or phrases
        //     3. categories

        //     Searches all args and suggest terms that would yield results
            
        //     */

            
            

        // }
        
        // [HttpGet("[action]")]
        // public JArray getWithCodes(string[] codes)
        // { 
        //     // like getAll() but returns args with specified codes
        // }
        [HttpGet("[action]")]
        public JArray getAll()
        { // return a short view of the argument

            JArray JsonArgs = new JArray();

            // for each arg, remove everything but the code and the payload, also add the 
            foreach (Arguments arg in Arguments.getAllArguments())
            {
                // get payload from each side
                Tuple<List<Arguments>, List<Arguments>> responses = arg.getArgumentsFromEachSide();
                JsonArgs.Add(
                    JObject.FromObject(
                        new
                        {
                            ArgumentPayload = arg.ArgumentPayload,
                            Code = arg.Code,
                            forArgs = shortenArgs(responses.Item1),
                            AgainstArgs = shortenArgs(responses.Item2)
                        })
                );
            }
            return JsonArgs;
        }

        [HttpGet("[action]")]
        public JObject getResponses(string code)
        {
            Arguments argument = Arguments.getWithCode(code);

            JArray forResponses = new JArray();
            foreach (var arg in argument.GetForResponses())
            {
                forResponses.Add(
                    JObject.FromObject(
                        new
                        {
                            code = arg.Code,
                            ArgumentPayload = arg.ArgumentPayload,
                            posterUsername = arg.getPosterUserName(),
                            upvotes = arg.getUpVotes(),
                            downvotes = arg.getDownVotes(),

                        })
                );
            }
            JArray AgainstArgs = new JArray();
            foreach (var arg in argument.GetAgainstResponses())
            {
                AgainstArgs.Add(
                    JObject.FromObject(
                        new
                        {
                            code = arg.Code,
                            ArgumentPayload = arg.ArgumentPayload,
                            posterUsername = arg.getPosterUserName(),
                            upvotes = arg.getUpVotes(),
                            downvotes = arg.getDownVotes(),

                        })
                );

            }
            return JObject.FromObject(new
            {
                againstResponses = AgainstArgs,
                forResponses = forResponses
            });
        }

        public JArray shortenArgs(List<Arguments> args)
        {
            // this function gets the responses and  shortens

            JArray shortArgs = new JArray();

            foreach (Arguments arg in args)
            {
                shortArgs.Add(JObject.FromObject(
                    new
                    {
                        Code = arg.Code,
                        ArgumentPayload = arg.ArgumentPayload

                    }));

            }


            return shortArgs;
        }

        [HttpGet("[action]")]
        public JObject getWithCode(string code)
        {
            Arguments arg = Arguments.getWithCode(code);

            //hide poster code // implement this properly 
            return JObject.FromObject(
                new
                {
                    ArgumentPayload = arg.ArgumentPayload,
                    posterUsername = arg.getPosterUserName(),
                    ArgumentExplanation = arg.ArgumentExplanation,
                    upvotes = arg.getUpVotes(),
                    downvotes = arg.getDownVotes(),
                    forArgs = shortenArgs(arg.GetForResponses()),
                    AgainstArgs = shortenArgs(arg.GetAgainstResponses())
                }
            );
        }

        [HttpPost("[action]")]
        public void createArg([FromBody]JObject bdy) // accept body object as json object from request body
        {
            /*
            Make chanhes so Arguments object can be accepted frombody
             */
            /*
            Use newtonsoft JOject to recieve the json object from the client as a json object 
            because passing in multiple values is not allowed a single object needs to bound to a type. Here I bind the body to a JObject and then retrieve neccasary data from this object
            */

            dbArgResolverContext context = new dbArgResolverContext();

            // this means its a response
            if (bdy["respondingToArg"].HasValues)
            {
                Arguments postArg = new Arguments(
                    Guid.NewGuid().ToString(),
                    (from usr in context.Users
                     where usr.Username == (string)bdy["Username"]
                     select usr.Code).FirstOrDefault(),
                    Arguments.getWithCode((string)bdy["respondingToArg"]["code"]).Category, // set the category to the parent arg
                    (string)bdy["ArgumentPayload"],
                    (string)bdy["ArgumentExplanation"],
                    DateTime.Now.AddMonths(2),
                    (bool)bdy["respondingToArg"]["for"],
                    bdy["respondingToArg"]["code"].ToString()
                );

                postArg.saveArgument();
            }
            else
            {
                Arguments postArg = new Arguments(
                              Guid.NewGuid().ToString(),
                              (from usr in context.Users
                               where usr.Username == (string)bdy["Username"]
                               select usr.Code).FirstOrDefault(),
                                "testInput",
                              (string)bdy["ArgumentPayload"],
                              (string)bdy["ArgumentExplanation"],
                              DateTime.Now.AddMonths(2)
                          );
                postArg.saveArgument();
            }





        }

    }

}
