SELECT
    *
FROM
    `dbArgResolver`.`Arguments`
LIMIT
    1000;
    
    
      SELECT
    *
FROM
    `dbArgResolver`.`Arguments`
LIMIT
    1000;
    
        DELETE FROM
        dbArgResolver.Arguments;

        0f91a215-3ab0-4f79-a31e-f6607eead3d9	pen	    pen
        12b1f1a6-e5a9-4a1c-85bf-5caa9faf0d6b	logan	logan
        3fd22b20-73a6-443c-8ade-9d4c2d1b8fa0	ocean	mrwavey
        56a15dc0-b9a1-4896-a408-aa4d5977688f	earnest	earnest
        760080e3-55b3-4dd5-b38b-852fe5afe047	edward	edwardo
        ffe75d09-c987-472a-9e2b-48fa9ab2ff02	maleni	male

        +---------------------+---------------+------+-----+---------+-------+
        | Field               | Type          | Null | Key | Default | Extra |
        +---------------------+---------------+------+-----+---------+-------+
        | code                | varchar(255)  | NO   | PRI | NULL    |       |
        | posterCode          | varchar(255)  | NO   | MUL | NULL    |       |
        | category            | varchar(255)  | YES  |     | NULL    |       |
        | numOfViews          | int(11)       | YES  |     | NULL    |       |
        | argumentPayload     | varchar(255)  | YES  |     | NULL    |       |
        | argumentExplanation | varchar(2000) | YES  |     | NULL    |       |
        | datePosted          | datetime      | YES  |     | NULL    |       |
        | closingDate         | datetime      | YES  |     | NULL    |       |
        | forArgument         | tinyint(1)    | NO   |     | 0       |       |
        | requestAccepted     | tinyint(1)    | YES  |     | NULL    |       |
        | respondingToCode    | varchar(255)  | YES  | MUL | NULL    |       |
        +---------------------+---------------+------+-----+---------+-------+
        
        INSERT INTO
    `dbArgResolver`.`Arguments` (
        code,
        posterCode,
        category,
        numOfViews,
        argumentPayload,
        argumentExplanation,
        datePosted,
        closingDate,
        forArgument,
        requestAccepted,
        respondingToCode
    ) VALUE(
        "itsUnavoidable",
        "12b1f1a6-e5a9-4a1c-85bf-5caa9faf0d6b",
        "test",
        0,
        "we should stop driving petrol because it will destroy the world",
        "Global warming occurs when carbon dioxide (CO2) and other air pollutants and greenhouse gases collect in the atmosphere and absorb sunlight and solar radiation that have bounced off the earth’s surface. Normally, this radiation would escape into space—but these pollutants, which can last for years to centuries in the atmosphere, trap the heat and cause the planet to get hotter. That's what's known as the greenhouse effect.",
        CURRENT_DATE(),
        DATE("2019-07-30"),
        TRUE,
        TRUE,
        "antiGlobalWarming"
    )
+--------------+--------------+------+-----+---------+-------+
| Field        | Type         | Null | Key | Default | Extra |
+--------------+--------------+------+-----+---------+-------+
| code         | varchar(255) | NO   | PRI | NULL    |       |
| username     | varchar(255) | NO   |     | NULL    |       |
| userPassword | varchar(255) | NO   |     | NULL    |       |
+--------------+--------------+------+-----+---------+-------+

INSERT INTO
    `dbArgResolver`.`Users` 
        (code,username,userPassword)
        VALUE
        ("123456","logan","logan")