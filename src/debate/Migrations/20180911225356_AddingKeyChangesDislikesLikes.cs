﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class AddingKeyChangesDislikesLikes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "Arguments_ibfk_1",
                table: "Arguments");

            migrationBuilder.DropForeignKey(
                name: "Evidence_ibfk_1",
                table: "Evidence");

            migrationBuilder.CreateTable(
                name: "ArgumentDislikes",
                columns: table => new
                {
                    userCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    argumentCode = table.Column<string>(type: "varchar(255)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArgumentDislikes", x => new { x.argumentCode, x.userCode });
                    table.ForeignKey(
                        name: "fkDislikedArgument",
                        column: x => x.argumentCode,
                        principalTable: "Arguments",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.AddForeignKey(
                name: "fkLikedArgument",
                table: "ArgumentLikes",
                column: "argumentCode",
                principalTable: "Arguments",
                principalColumn: "code",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fkPosterCode",
                table: "Arguments",
                column: "posterCode",
                principalTable: "Users",
                principalColumn: "code",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fkEvidencesArgument",
                table: "Evidence",
                column: "argumentCode",
                principalTable: "Arguments",
                principalColumn: "code",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fkLikedArgument",
                table: "ArgumentLikes");

            migrationBuilder.DropForeignKey(
                name: "fkPosterCode",
                table: "Arguments");

            migrationBuilder.DropForeignKey(
                name: "fkEvidencesArgument",
                table: "Evidence");

            migrationBuilder.DropTable(
                name: "ArgumentDislikes");

            migrationBuilder.AddForeignKey(
                name: "Arguments_ibfk_1",
                table: "Arguments",
                column: "posterCode",
                principalTable: "Users",
                principalColumn: "code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "Evidence_ibfk_1",
                table: "Evidence",
                column: "argumentCode",
                principalTable: "Arguments",
                principalColumn: "code",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
