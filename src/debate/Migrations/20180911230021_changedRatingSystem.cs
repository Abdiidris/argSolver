﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class changedRatingSystem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArgumentDislikes");

            migrationBuilder.DropTable(
                name: "ArgumentLikes");

            migrationBuilder.CreateTable(
                name: "ArgumentRating",
                columns: table => new
                {
                    userCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    argumentCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    up = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArgumentRating", x => new { x.argumentCode, x.userCode });
                    table.ForeignKey(
                        name: "fkRatedArgument",
                        column: x => x.argumentCode,
                        principalTable: "Arguments",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArgumentRating");

            migrationBuilder.CreateTable(
                name: "ArgumentDislikes",
                columns: table => new
                {
                    argumentCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    userCode = table.Column<string>(type: "varchar(255)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArgumentDislikes", x => new { x.argumentCode, x.userCode });
                    table.ForeignKey(
                        name: "fkDislikedArgument",
                        column: x => x.argumentCode,
                        principalTable: "Arguments",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ArgumentLikes",
                columns: table => new
                {
                    argumentCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    userCode = table.Column<string>(type: "varchar(255)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArgumentLikes", x => new { x.argumentCode, x.userCode });
                    table.ForeignKey(
                        name: "fkLikedArgument",
                        column: x => x.argumentCode,
                        principalTable: "Arguments",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });
        }
    }
}
