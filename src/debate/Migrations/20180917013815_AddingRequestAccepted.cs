﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class AddingRequestAccepted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "up",
                table: "Responses");

            migrationBuilder.AlterColumn<bool>(
                name: "requestAccepted",
                table: "Responses",
                type: "Bool",
                nullable: true,
                oldClrType: typeof(bool),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "requestAccepted",
                table: "Responses",
                nullable: true,
                oldClrType: typeof(bool),
                oldType: "Bool",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "up",
                table: "Responses",
                nullable: false,
                defaultValue: false);
        }
    }
}
