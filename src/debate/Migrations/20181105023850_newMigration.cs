﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class newMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fkResponseCode",
                table: "ContributionRequests");

            migrationBuilder.DropTable(
                name: "Responses");

            migrationBuilder.AddColumn<bool>(
                name: "forArgument",
                table: "Arguments",
                type: "Bool",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "requestAccepted",
                table: "Arguments",
                type: "Bool",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "fkResponseCode",
                table: "ContributionRequests",
                column: "responseCode",
                principalTable: "Arguments",
                principalColumn: "code",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fkResponseCode",
                table: "ContributionRequests");

            migrationBuilder.DropColumn(
                name: "forArgument",
                table: "Arguments");

            migrationBuilder.DropColumn(
                name: "requestAccepted",
                table: "Arguments");

            migrationBuilder.CreateTable(
                name: "Responses",
                columns: table => new
                {
                    code = table.Column<string>(nullable: false),
                    RespondingToArgmentCode = table.Column<string>(nullable: true),
                    forArgument = table.Column<bool>(type: "Bool", nullable: false),
                    requestAccepted = table.Column<bool>(type: "Bool", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Responses", x => x.code);
                    table.ForeignKey(
                        name: "ArgumentResponse_ibfk_1",
                        column: x => x.RespondingToArgmentCode,
                        principalTable: "Arguments",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Responses_RespondingToArgmentCode",
                table: "Responses",
                column: "RespondingToArgmentCode");

            migrationBuilder.AddForeignKey(
                name: "fkResponseCode",
                table: "ContributionRequests",
                column: "responseCode",
                principalTable: "Responses",
                principalColumn: "code",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
