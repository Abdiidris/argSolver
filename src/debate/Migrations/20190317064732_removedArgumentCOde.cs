﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class removedArgumentCOde : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "argumentCode",
                table: "ContributionRequests",
                newName: "accept");

            migrationBuilder.AlterColumn<bool>(
                name: "accept",
                table: "ContributionRequests",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "accept",
                table: "ContributionRequests",
                newName: "argumentCode");

            migrationBuilder.AlterColumn<string>(
                name: "argumentCode",
                table: "ContributionRequests",
                type: "varchar(255)",
                nullable: true,
                oldClrType: typeof(bool),
                oldNullable: true);
        }
    }
}
