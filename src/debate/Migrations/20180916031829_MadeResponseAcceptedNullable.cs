﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class MadeResponseAcceptedNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "requestAccepted",
                table: "Responses",
                nullable: true,
                oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "requestAccepted",
                table: "Responses",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);
        }
    }
}
