﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class addingConReq : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contributers");

            migrationBuilder.AddColumn<bool>(
                name: "requestAccepted",
                table: "Responses",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "ContributionRequests",
                columns: table => new
                {
                    responseCode = table.Column<string>(nullable: false),
                    voterCode = table.Column<string>(nullable: false),
                    argumentCode = table.Column<string>(type: "varchar(255)", nullable: true),
                    UsersCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContributionRequests", x => new { x.voterCode, x.responseCode });
                    table.ForeignKey(
                        name: "FK_ContributionRequests_Users_UsersCode",
                        column: x => x.UsersCode,
                        principalTable: "Users",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fkResponseCode",
                        column: x => x.responseCode,
                        principalTable: "Responses",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContributionRequests_UsersCode",
                table: "ContributionRequests",
                column: "UsersCode");

            migrationBuilder.CreateIndex(
                name: "IX_ContributionRequests_responseCode",
                table: "ContributionRequests",
                column: "responseCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContributionRequests");

            migrationBuilder.DropColumn(
                name: "requestAccepted",
                table: "Responses");

            migrationBuilder.CreateTable(
                name: "Contributers",
                columns: table => new
                {
                    argumentCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    userCode = table.Column<string>(type: "varchar(255)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contributers", x => new { x.argumentCode, x.userCode });
                });
        }
    }
}
