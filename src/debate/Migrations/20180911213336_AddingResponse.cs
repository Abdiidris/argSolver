﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class AddingResponse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

          

//-------
            migrationBuilder.CreateTable(
                name: "ArgumentLikes",
                columns: table => new
                {
                    userCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    argumentCode = table.Column<string>(type: "varchar(255)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArgumentLikes", x => new { x.argumentCode, x.userCode });
                });

            migrationBuilder.CreateTable(
                name: "Contributers",
                columns: table => new
                {
                    argumentCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    userCode = table.Column<string>(type: "varchar(255)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contributers", x => new { x.argumentCode, x.userCode });
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    TestProperty = table.Column<string>(nullable: true),
                    code = table.Column<string>(type: "varchar(255)", nullable: false),
                    username = table.Column<string>(type: "varchar(255)", nullable: false),
                    userPassword = table.Column<string>(type: "varchar(255)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.code);
                });

            migrationBuilder.CreateTable(
                name: "Arguments",
                columns: table => new
                {
                    code = table.Column<string>(type: "varchar(255)", nullable: false),
                    posterCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    category = table.Column<string>(type: "varchar(255)", nullable: true),
                    numOfViews = table.Column<int>(type: "int(11)", nullable: true),
                    argumentPayload = table.Column<string>(type: "varchar(255)", nullable: true),
                    argumentExplanation = table.Column<string>(type: "varchar(255)", nullable: true),
                    datePosted = table.Column<DateTime>(type: "datetime", nullable: true),
                    closingDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Arguments", x => x.code);
                    table.ForeignKey(
                        name: "Arguments_ibfk_1",
                        column: x => x.posterCode,
                        principalTable: "Users",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Evidence",
                columns: table => new
                {
                    code = table.Column<string>(type: "varchar(255)", nullable: false),
                    argumentCode = table.Column<string>(type: "varchar(255)", nullable: false),
                    evidencePayload = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Evidence", x => x.code);
                    table.ForeignKey(
                        name: "Evidence_ibfk_1",
                        column: x => x.argumentCode,
                        principalTable: "Arguments",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Responses",
                columns: table => new
                {
                    code = table.Column<string>(nullable: false),
                    RespondingToArgmentCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Responses", x => x.code);
                    table.ForeignKey(
                        name: "ArgumentResponse_ibfk_1",
                        column: x => x.RespondingToArgmentCode,
                        principalTable: "Arguments",
                        principalColumn: "code",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "posterCode",
                table: "Arguments",
                column: "posterCode");

            migrationBuilder.CreateIndex(
                name: "argumentCode",
                table: "Evidence",
                column: "argumentCode");

            migrationBuilder.CreateIndex(
                name: "IX_Responses_RespondingToArgmentCode",
                table: "Responses",
                column: "RespondingToArgmentCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArgumentLikes");

            migrationBuilder.DropTable(
                name: "Contributers");

            migrationBuilder.DropTable(
                name: "Evidence");

        
            migrationBuilder.DropTable(
                name: "Arguments");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
