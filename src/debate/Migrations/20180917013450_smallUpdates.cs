﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class smallUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TestProperty",
                table: "Users");

            migrationBuilder.AddColumn<bool>(
                name: "up",
                table: "Responses",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "up",
                table: "Responses");

            migrationBuilder.AddColumn<string>(
                name: "TestProperty",
                table: "Users",
                nullable: true);
        }
    }
}
