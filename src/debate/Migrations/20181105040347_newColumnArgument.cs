﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace debate.Migrations
{
    public partial class newColumnArgument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "respondingToCode",
                table: "Arguments",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Arguments_respondingToCode",
                table: "Arguments",
                column: "respondingToCode");

            migrationBuilder.AddForeignKey(
                name: "ArgumentResponse_ibfk_1",
                table: "Arguments",
                column: "respondingToCode",
                principalTable: "Arguments",
                principalColumn: "code",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "ArgumentResponse_ibfk_1",
                table: "Arguments");

            migrationBuilder.DropIndex(
                name: "IX_Arguments_respondingToCode",
                table: "Arguments");

            migrationBuilder.DropColumn(
                name: "respondingToCode",
                table: "Arguments");
        }
    }
}
