import React, { Component } from 'react';
import './NavBar.css';
class NavBar extends Component {

    render() {
        let loginOption;

        if (this.props.username === "") {
            loginOption = (<li className="nav-item">
                <a id="loginButton" className="nav-item btn btn-outline-primary ml-2" >Login</a>
            </li>)
        }
        else {
            loginOption = (<li className="nav-item">
                <a id="loginButton" className="nav-item btn btn-outline-danger ml-2 mr-1 " >logout: {this.props.username}</a>
            </li>);
        }

        return (
            <nav className="navbar navbar-expand-sm navbar-light">
                <a>
                    <div id="logo" className="navbar-brand bg-warning border border-dark">

                        <img src="logo.png" width="30" height="30" className="d-inline-block align-top" alt="" />
                        Debate
                </div>
                </a>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">

                        <li className="nav-item active">
                            <a id="home" className="btn btn-outline-primary" >Home</a>
                        </li>

                        <li className="nav-item active">
                            <a className="btn btn-outline-primary ml-2">Help</a>
                        </li>
                        <li id="new" className="nav-item">
                            <a className="nav-item btn btn-outline-success ml-auto">New</a>
                        </li>
                        {loginOption}
                        <li className="nav-item">
                            <form className="form-inline">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                                <button name="Done" id="search" className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </li>
                  
                    </ul>


                </div>
            </nav>
        );

    }


}

export default NavBar;