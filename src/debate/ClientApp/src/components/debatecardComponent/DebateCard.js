import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import ViewDebate from '../ViewDebateComponent/ViewDebate';
import './DebateCard.css';
class DebateCard extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {

        ReactDOM.render(
            <ViewDebate code={this.props.Code} />,
            document.getElementById('root')
        );
    }
    render() { 
        return (

            <div className="row justify-content-md-center" id="debateContainer">
                <div id="debate" className="col-sm-6 col-md-7 border border-dark text-center">
                    <div id="debatePointRow">
                        <h5 onClick={this.handleClick} id="debatePoint">
                            <em>
                                {this.props.debatePoint}
                            </em>
                        </h5>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="border border-dark text-light bg-primary responseHeading"> </div>

                            {this.props.forDebate.map(function (x) {
                                return (<div key={x.Code} className="border border-dark response">{x.ArgumentPayload}</div>)
                            })}

                        </div>
                        <div className="col-sm-6 ">
                            <div className="border border-dark text-light bg-success responseHeading">  </div>

                            {this.props.againstDebate.map(function (x) {
                                return (<div key={x.Code} class="border border-dark response">{x.ArgumentPayload}</div>)
                            })}

                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default DebateCard;