import React, { Component } from 'react';
import DebateCard from './DebateCard.js';
import axios from 'axios';
import './DebateCardList.css';
class DebateCardList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            debateList: []
        }

       
    }


     componentWillMount = () => {
        axios.get("https://localhost:5001/api/Arguments/getAll")
            .then((response => {
                
                console.log(response.data);
                this.setState({
                    isLoading: false,
                    debateList: response.data
                });
            }
            )).catch(err => {

                document.getElementById('root').innerHTML = err;
                console.log(err);

            });

    }


    render() {
      
        if (this.state.isLoading) {
            return (<div className="spinner-border" style={{ width: '10rem', height: '10rem', margin: '30%', marginLeft: '50%', backgroundColor: 'deepskyblue' }} role="status" >
                <span className="sr-only">Loading...</span>
            </div >);
        }
        else {
            return (<ul id="debateList" className="list-group">
                {this.state.debateList.map(debate => (
                    <li className="list-item">
                        {<DebateCard Code={debate.Code} debatePoint={debate.ArgumentPayload} forDebate={debate.forArgs} againstDebate={debate.AgainstArgs} />}
                    </li>
                ))}
            </ul>);
        }
    }
}

export default DebateCardList;