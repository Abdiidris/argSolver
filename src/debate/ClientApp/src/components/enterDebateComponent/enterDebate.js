import React, { Component } from 'react';
import axios from 'axios';
import jsco from 'js-cookie'
import ReactDOM from 'react-dom';

import DebateCardList from '../debatecardComponent/DebateCardList';
import './enterDebate.css';
class EnterDebate extends Component {

    constructor(props) {
        super(props);
        this.addDebate = this.addDebate.bind(this);
    }
    addDebate(event) {

        var form = event.target;

        if (form.point.value !== '' && jsco.get('currentUsername') !== '') {

            // post the debate
            var respondingTo = {}
            if (this.props.isResponse) {
                respondingTo = {
                    code: this.props.respondingToArgCode,
                    for: (form.sidePick.value === 'for')
                };
            }
            axios.post('https://localhost:5001/api/Arguments/createArg', {

                ArgumentPayload: form.point.value,
                ArgumentExplanation: form.explanation.value,
                Username: jsco.get('currentUsername'),
                respondingToArg: respondingTo

            }).then(response => {
                ReactDOM.render(
                    <DebateCardList />,
                    document.getElementById('root')
                );
            })
                .catch(err => {
                    document.getElementById('root').innerHTML = err.response.data;
                    alert(err);
                });


        } else {
            alert('not entered properly');
        }

    }
    render() {
        var responseDiv = '';
        if (this.props.isResponse) {
            responseDiv = (<div className=" border border-dark p-4" >
                <p className=" w-100 p-2 mb-2">  {this.props.respondingToArg}</p>
                <div class="form-check ml-3">
                    <input class="form-check-input" name="sidePick" type="radio" value="for" />
                    <label className="form-check-label"> For </label>
                </div>
                <div class="form-check  ml-3">
                    <input class="form-check-input" name="sidePick" type="radio" value="Against" />
                    <label className="form-check-label"> Against </label>
                </div>
            </div>)
        }

        return (


            <form onSubmit={this.addDebate} className="ml-2" id="enterDebateForm" action="">
                {responseDiv}
                <div className="form-group row">
                    <label className="col-form-label"> Debate Point: </label>
                    <input name="point" type="text" className="form-control" placeholder="enter here" />
                </div>

                <div className="form-group row">
                    <label>Explanation:</label>
                    <textarea type="text" className="form-control" name="explanation" rows="3" placeholder="I thought love was only true in fairy tales Meant for someone else but not for me Love was out to get me That's the way it seemed Disappointment haunted all of my dreams Then I saw her face, now I'm a believer Not a trace of doubt in my mind I'm in love I'm a believer, I couldn't leave her if I tried I thought love was more or less a giving thing Seems the more I gave the less I got What's the use in tryin' All you get is pain? When I needed sunshine, I got rain"></textarea>
                </div>

                <input value="Submit" id="postDebate" className="btn btn-outline-success my-2 my-sm-0" type="submit" />
            </form>
        );
    }
}

export default EnterDebate;