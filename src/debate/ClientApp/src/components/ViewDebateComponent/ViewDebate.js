import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import './ViewDebate.css';
import axios from 'axios';
import background from '../img/d.jpg';
import EnterDebate from '../enterDebateComponent/enterDebate';
import Responses from '../responsesComponent/Responses'

class ViewDebate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            code: this.props.code,
            arg: {}
        }

        this.respond = this.respond.bind(this);

    }
    componentDidMount = () => {

        axios.get('https://localhost:5001/api/Arguments/getWithCode?code=' + this.props.code + '')

            .then(response => {
                console.log(response.data);
                this.setState({

                    isLoading: false,
                    Code: this.props.code,
                    arg: response.data

                });
                console.log(response);
            })
            .catch(err => console.log(err));
    }
    respond(e) {
        ReactDOM.render(
            <EnterDebate isResponse={true} respondingToArg={this.state.arg.ArgumentPayload} respondingToArgCode={this.props.code} />,
            document.getElementById('root')
        );
    }
    render() {

        if (this.state.isLoading) {
            return (<div className="spinner-border" style={{ width: '10rem', height: '10rem', margin: '30%', marginLeft: '50%', backgroundColor: 'deepskyblue' }} role="status" >
                <span className="sr-only">Loading...</span>
            </div >);
        }
        else {
            return (
                <div className="container">

                    <div className="row" id="info">
                        <div className="col-sm-7 text-uppercase">{this.state.arg.posterUsername}</div>

                        <button className="col-sm-1 btn btn-outline-success text-center ml-auto">
                            {this.state.arg.upvotes}
                        </button>
                        <button className="col-sm-1 btn btn-outline-danger text-center ml-sm-2">
                            {this.state.arg.downvotes}
                        </button>
                        <button onClick={this.respond} className="col-sm-2 btn bg-primary text-center text-white ml-sm-4">
                            Respond
                        </button>

                    </div>

                    <div className="row card bg-dark text-white text-center mt-4" id="Title">
                        <img className="card-img" src={background} alt="Card image" id="backgroundImage" />
                        <div className="card-img-overlay">
                            <h5 className="card-title text-justify">{this.state.arg.ArgumentPayload}</h5>

                        </div>
                    </div>


                    <p className="row border border-dark text-justify" id="explanation">
                        {this.state.arg.ArgumentExplanation} 
                    </p>
                
                    <div className="row mt-1">
                        {<Responses code={this.props.code} />}
                    </div>

                </div>


            );
        }

    }
}

export default ViewDebate;