import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './ResponseCard.css';
import ViewDebate from '../ViewDebateComponent/ViewDebate';
class ResponseCard extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {

        ReactDOM.render(
            <ViewDebate code={this.props.response.code} />,
            document.getElementById('root')
        );
    }
    render() {
        return (

            <div className="container border border-dark rounded">

                <div onClick={this.handleClick} className="row text-justify response p-2 h-75">
                    {this.props.response.ArgumentPayload}
                </div>

                <div className="row p-1 pl-3 pr-3 mt-1" style={{ backgroundColor: "#e6e6e6" }}>
                    <div className="col-sm-6">
                        <em>
                            {this.props.response.posterUsername}
                        </em>
                    </div>

                    <div className="col-sm-2 rounded mt-1 mr-2 ml-auto text-center text-success">
                        {this.props.response.upvotes}
                    </div>

                    <div className="col-sm-2 mt-1 rounded text-center text-danger">
                        {this.props.response.downvotes}
                    </div>
                </div>
            </div>);
    }
}

export default ResponseCard;