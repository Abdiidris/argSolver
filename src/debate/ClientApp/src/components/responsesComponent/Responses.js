import React, { Component } from 'react';
import axios from 'axios';

import ResponseCard from './ResponseCard';
class Responses extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            forResponses: [],
            AgainstResponses: []
        }



    }
    componentDidMount = () => {

        axios.get('https://localhost:5001/api/Arguments/getResponses?code=' + this.props.code + '')

            .then(response => {
                console.log(response.data);
                this.setState({

                    isLoading: false,
                    forResponses: response.data.forResponses,
                    againstResponses: response.data.againstResponses,

                });

                console.log(response);
            })
            .catch(err => console.log(err));
    }



    render() {

        if (this.state.isLoading) {
            return (<div className="spinner-border" style={{ width: '10rem', height: '10rem', margin: '30%', marginLeft: '50%', backgroundColor: 'deepskyblue' }} role="status" >
                <span className="sr-only">Loading...</span>
            </div >);
        }
        else {

            return (
                <div className="container">
                    <div className="row mt-2">
                        <div className="col-sm-6 p-2" style={{ backgroundColor: '#ffffff' }}>
                            <ul className="list-group">
                                {
                                    this.state.forResponses.map(x => (
                                        <li className="list-item mt-2">
                                            <ResponseCard response={x} />
                                        </li>
                                    ))
                                }

                            </ul>
                        </div>

                        <div className="col-sm-6 p-2" style={{ backgroundColor: '#ffffff' }}>
                            <ul className="list-group">
                                {
                                    this.state.againstResponses.map(x => (
                                        <li className="list-item mt-2">
                                            <ResponseCard response={x} />
                                        </li>
                                    ))
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            )
        }





    }

}


export default Responses;