import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import jsco from 'js-cookie';

import './index.css';
import DebateCardList from './components/debatecardComponent/DebateCardList';
import NavBar from './components/navigationComponent/NavBar';
import EnterDebate from './components/enterDebateComponent/enterDebate';
import ViewDebate from './components/ViewDebateComponent/ViewDebate';
import Login from './components/loginComponent/Login'

openNavbar();

window.onload = function () {
 
  //openAllDebates();
  document.getElementById('home').onclick = openAllDebates;
  document.getElementById('new').onclick = newDebate;
  document.getElementById('loginButton').onclick = loginPage;


}

function loginPage() {
  // if the user hasnt logged in
  if (jsco.get('currentUsername') === '') {
    ReactDOM.render(
      <Login />,
      document.getElementById('root')
    );

    document.forms['loginForm'].onsubmit = function () {
      jsco.set('currentUsername', document.forms['loginForm'].username.value)
    };
  }
  else {
    jsco.set('currentUsername', '');
    openNavbar(); // reload the navbar
  }

}

function newDebate() {
  ReactDOM.render(
    <EnterDebate isResponse={false}/>,
    document.getElementById('root')
  );
 }



function openAllDebates() {
  ReactDOM.render(
    <DebateCardList />,
    document.getElementById('root')
  );
}

function openDebate(code) {
  ReactDOM.render(
    <ViewDebate code={code} />,
    document.getElementById('root')
  );
}

function openNavbar() {
  ReactDOM.render(
    <NavBar username={jsco.get('currentUsername')} />,
    document.getElementById('navBar')
  );

}
